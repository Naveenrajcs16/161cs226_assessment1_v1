<%-- 
    Document   : index
    Created on : Aug 15, 2018, 5:26:00 PM
    Author     : NaveenRaj
--%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Feedback</title>
	<style type="text/css">
		body{
			background: #fafcfc;
		}
		#wrapper
		{
			width:50%;
			height:auto;
			margin:10px auto;
			border:1px solid #cbcbcb;
			background: white;
                        
                      
		}
                #display_area{
                    width:50%;
			height:auto;
			margin:10px auto;
			border:1px solid #cbcbcb;
			background: white;
           
                }
		#comment_form li{
		list-style-type: none;
		margin:5px;
		}
		#comment_form ,result
		{
			width:50%;
			margin: 100px auto;
			background: #fafcfc;
			padding:20px;
		}
		#name,textarea{
			width:80%;
		}
	</style>
	
</head>
<body>
<div id="wrapper">
            
         <div id="comment_form">
	<li>Name :</li>
	<li><input type="text" id="name"></li>
	<li>Comment: </li>
	<li><textarea id="comment"></textarea></li>
	<li><input type="submit" id="submit" value="POST"></li>
         </div>
            <table><tr><td id="result"></td></tr></table>
</div>
<div id="display_area"></div>
</body>
</html>
<script type="text/javascript" src="jquery-3.3.1.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
    
      display();
    $("#submit").click(function(){
         var name=$("#name").val();
         var comment=$("#comment").val();
         var inputData="name="+name+"&comment_text="+comment;
	$.ajax(
                {
	url:"Process",type:"POST",async:false,
	data:inputData,
	success:function (data)
                    {
                          display();
	        $('#name').val('');
                         $("#comment").val('');
	  }
	})
         });
});
function display(){
     $.ajax({
     url:"displayJSP.jsp", type:"POST",
     success:function(data)
     {
         console.log("In display");
         $("#display_area").html(data);
     }
 });
}
               
</script>