<%-- 
    Document   : displayJSP
    Created on : Aug 15, 2018, 11:24:56 PM
    Author     : NaveenRaj
--%>

<%@page import="fb.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback Page</title>
        <style>
            #text{
                width:90%;
                margin:5px;
                border:1px solid #cbcbcb;
                padding:10px;
               
            }
            #un{
                color:blue;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <%
            DBConnect con = new DBConnect();
            con.getConnection();
             con.read1("SELECT * FROM feedback.fbs");
             while(con.rs1.next())
             {
           String username= con.rs1.getString("username");
           String comment=con.rs1.getString("comments");
        %>
        <div id='text'>
        <h5 id="un"><%=username%></h5>
        <h5 id='cmt'><%=comment%></h5>
        </div>
               <% }
        %>
    </body>
</html>
